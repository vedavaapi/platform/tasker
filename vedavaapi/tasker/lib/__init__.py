from collections import OrderedDict
from datetime import datetime
from typing import Optional, List, Dict

import bcrypt
import glom
import jsonschema
from vedavaapi.acls.permissions_helper import PermissionResolver

from vedavaapi.common.helpers.models import VVContext, AgentContext
from vedavaapi.objectdb.helpers import projection_helper
from vedavaapi.objectdb.mydb import MyDbCollection
from vedavaapi.objectdb.helpers.objstore_helper import ObjModelException


class TaskManager(object):

    STATES = ['PENDING', 'PROGRESS', 'PARTIAL', 'SUCCESS', 'FAILURE']
    RESOLVED_STATES = STATES[2:]

    COUNT_LIMIT = 100
    MAX_COUNT_FULL_FILL_ITERS = 3

    def __init__(self, vv_context: VVContext, tasks_colln: MyDbCollection):
        self.vv_context = vv_context
        self.tasks_colln = tasks_colln

    def _check_permission(
            self, target_id, actions, agent_context: AgentContext,
            res_cache=None, acls_cache=None,
    ):
        target = self.vv_context.colln.get(
            target_id,
            projection={
                "jsonClass": 1, "_id": 1,
                "source": 1, "target": 1, "hierarchy": 1
            }
        )
        if not target:
            raise ObjModelException('invalid target identifier', 400)
        resolved_permissions = PermissionResolver.get_resolved_permissions(
            self.vv_context.colln, self.vv_context.acl_svc,
            target, actions, agent_context.user_id, agent_context.team_ids,
            resource_graph_cache=res_cache, acls_cache=acls_cache
        )
        if False in resolved_permissions.values():
            raise ObjModelException('permission denied', 403)

    def register_task(
            self, task, agent_context: AgentContext,
            res_cache=None, acls_cache=None):
        task['jsonClass'] = 'Task'
        if 'agent' in task or 'resolved' in task or 'chronology' in task:
            raise ObjModelException(
                'cannot set custom agent/resolved/chronology fields', 400)
        target_id = glom.glom(task, 'on.target.id', default=None)
        if not target_id:
            raise ObjModelException('invalid target', 400)
        actions = glom.glom(task, 'actions', default=None)
        if not actions or not set(actions).issubset(PermissionResolver.ACTIONS):
            raise ObjModelException('invalid task actions', 400)
        state = glom.glom(task, 'status.state', default=None)
        if not state:
            raise ObjModelException('invalid task state', 400)

        now = str(datetime.now())
        task['created'] = now
        glom.assign(task, 'chronology.started_at', now, missing=dict)
        glom.assign(task, 'chronology.updated_at', now, missing=dict)
        glom.assign(
            task, 'resolved', state in self.RESOLVED_STATES, missing=dict)
        glom.assign(task, 'agent.user.id', agent_context.user_id, missing=dict)
        glom.assign(
            task, 'agent.teams',
            list(map(lambda _id: {"id": _id}, agent_context.team_ids or [])),
            missing=dict
        )

        try:
            if self.vv_context.schema_validator:
                self.vv_context.schema_validator.validate(task)
        except jsonschema.exceptions.ValidationError as e:
            raise ObjModelException(
                f'Invalid task:{e.message}', http_response_code=400)
        
        self._check_permission(
            target_id, actions, agent_context,
            res_cache=res_cache, acls_cache=acls_cache)

        task['task_token'] = bcrypt.gensalt(12).decode(
            'utf-8').rsplit('$', 1)[-1]

        task_id = self.tasks_colln.insert_one(task).inserted_id
        return {"task_id": task_id, "task_token": task['task_token']}

    def register_tasks(self, tasks: List[Dict], agent_context: AgentContext):
        reg_task_infos = []
        res_cache = {}
        acls_cache = {}
        for task in tasks:
            try:
                task_info = self.register_task(
                    task, agent_context,
                    res_cache=res_cache, acls_cache=acls_cache)
                reg_task_infos.append({"status": "SUCCESS", "info": task_info})
                
            except ObjModelException as e:
                reg_task_infos.append({
                    "status": "FAILURE",
                    "error": {"message": e.message, "code": e.http_response_code}
                })
        return reg_task_infos

    def update_task_status(
            self, task_token, task_state, task_meta=None, work_update=None):
        task = self.tasks_colln.find_one({'task_token': task_token})
        if not task:
            raise ObjModelException('invalid task token', 400)

        if task.get('resolved'):
            raise ObjModelException('task is already resolved', 403)

        if task_state not in self.STATES:
            raise ObjModelException('invalid task state', 400)

        now = str(datetime.now())
        task_update = {
            f'status.{k}': v for k, v in task_meta.items()} if task_meta else {}
        task_update.update(
            {'status.state': task_state, 'chronology.updated_at': now})
        if work_update:
            for f in ['succeeded', 'failed', 'quantity']:
                if f in work_update:
                    if type(work_update[f]) != int:
                        raise ObjModelException('invalid work update', 400)
                    task_update[f'work.{f}'] = work_update[f]
        if task_state in self.RESOLVED_STATES:
            task_update['resolved'] = True
            task_update['chronology.resolved_at'] = now

        update_directives = {"$set": task_update}
        if task_state in self.RESOLVED_STATES:
            update_directives['$unset'] = {"task_token": ''}

        self.tasks_colln.update_item(task['_id'], {"$set": task_update})

    def _get_readable_tasks(
            self, selector_doc, start: int, max_count: int,
            sort_doc: Optional[dict], agent_context: AgentContext,
            projection=None, res_graph_cache=None, acls_cache=None):

        res_graph_cache = res_graph_cache if res_graph_cache is not None else {}
        acls_cache = acls_cache if acls_cache is not None else {}

        ops = OrderedDict()
        if sort_doc:
            ops['sort'] = [sort_doc]
        ops['skip'] = [start or 0]
        ops['limit'] = [max_count]

        print(ops)

        tasks = self.tasks_colln.find_and_do(selector_doc, ops, projection=projection)  # type: list
        new_target_ids = set()
        for task in tasks:
            target_id = glom.glom(task, 'on.target.id')
            if target_id in res_graph_cache:
                continue
            new_target_ids.add(target_id)

        if len(new_target_ids):
            new_resources = self.vv_context.colln.find(
                {"_id": {"$in": list(new_target_ids)}},
                projection={"_id": 1, "jsonClass": 1, "source": 1, "target": 1}
            )
            resolved_permissions_list = PermissionResolver.get_resolved_permissions_for_resources(
                self.vv_context.colln, self.vv_context.acl_svc, new_resources,
                PermissionResolver.ACTIONS, agent_context.user_id,
                agent_context.team_ids,
                resource_graph_cache=res_graph_cache, acls_cache=acls_cache
            )
            for i, res in enumerate(new_resources):
                resolved_permissions = resolved_permissions_list[i]
                res['resolvedPermissions'] = resolved_permissions

        readable_tasks = []
        for task in tasks:
            resolved_perms = glom.glom(
                res_graph_cache,
                f'{glom.glom(task, "on.target.id")}.resolvedPermissions',
                default={},
            )
            if not resolved_perms[PermissionResolver.READ]:
                continue
            task['resolvedPermissions'] = resolved_perms
            readable_tasks.append(task)

        return readable_tasks

    def get_tasks(
            self, selector_doc, start, count,
            sort_doc, agent_context: AgentContext,
            projection=None, return_count_only=False):
        if not count or count > 100:
            count = 100
        projection_helper.validate_projection(projection)
        projection = projection_helper.get_restricted_projection(
            projection, exposed_projection={"task_token": 0}
        )

        total = self.tasks_colln.count(selector_doc)
        if count > total:
            count = total

        if return_count_only:
            return {"total_count": total, "items": []}

        effective_count = 0
        iters = 0
        readable_tasks = []
        res_cache = {}
        acls_cache = {}
        while (effective_count < count) and (iters < self.MAX_COUNT_FULL_FILL_ITERS):
            iter_count = count - len(readable_tasks)
            #  print(effective_count, iter_count, len(readable_tasks), count, total, iters)
            iter_tasks = self._get_readable_tasks(
                selector_doc, start + effective_count, iter_count,
                sort_doc, agent_context,
                projection=projection, res_graph_cache=res_cache, acls_cache=acls_cache
            )
            readable_tasks.extend(iter_tasks)
            effective_count += iter_count
            iters += 1

        #  print(effective_count, iters, len(readable_tasks), count, total)

        return {
            "total_count": total - (
                (effective_count - count) if (effective_count > count) else 0),
            "count": len(readable_tasks),
            "start": start,
            "effective_count": effective_count,
            "items": readable_tasks
        }
