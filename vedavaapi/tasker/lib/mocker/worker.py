import os
import time
import uuid
from collections import OrderedDict

import glom

from vedavaapi.common.helpers.models import VVContext, VVContextDescriptor, AgentContext
from vedavaapi.common.helpers.celery import celery as celery_app
from vedavaapi.objectdb.helpers import objstore_helper, ObjModelException


@celery_app.task(bind=True)
def sleep_for_res_task(
        self, vv_context_descriptor: VVContextDescriptor, agent_context: AgentContext,
        reg_task_info: dict,
):
    vv_context = VVContext.from_descriptor(vv_context_descriptor)
    vv_context.task_manager.update_task_status(reg_task_info['task_token'], 'PROGRESS')
    for i in range(0, 100):
        time.sleep(2)
        self.update_state(
            state='PROGRESS', meta={"status": "sleeping", "current": i, "total": 100})
    success_meta = {"status": "completed", "current": 100, "total": 100}
    vv_context.task_manager.update_task_status(
        reg_task_info['task_token'], 'SUCCESS', task_meta=success_meta)
    return success_meta


@celery_app.task(bind=True)
def sleep_for_children_task(
        self, vv_context_descriptor: VVContextDescriptor, agent_context: AgentContext,
        reg_task_info: dict, res_id: str, filter_doc, task_route_base
):
    vv_context = VVContext.from_descriptor(vv_context_descriptor)
    vv_context.task_manager.update_task_status(reg_task_info['task_token'], 'PROGRESS')  # meta task status update

    start = 0
    count = 10
    cumulative_count = start

    res_cache = {}
    acls_cache = {}

    selector_doc = filter_doc.copy()
    selector_doc['source'] = res_id

    total_count = vv_context.colln.count(selector_doc)
    failed_children = []  # in registering task
    succeded_children = []

    while cumulative_count < total_count:
        ops = OrderedDict()
        ops['skip'] = [start]
        ops['limit'] = [count]
        iter_tasks = []
        children = []
        try:
            children = objstore_helper.get_read_permitted_resources(
                vv_context.colln, vv_context.acl_svc,
                agent_context.user_id, agent_context.team_ids,
                selector_doc=selector_doc, projection={"_id": 1, "jsonClass": 1},
                ops=ops, resource_graph_cache=res_cache, acls_cache=acls_cache
            )

            for c in children:
                task_id = uuid.uuid4().hex
                task = {
                    "jsonClass": "Task",
                    "type":"rdfs:Resource",
                    "name": "Mock task",
                    "work": {"work_type": "sleep", "quantum": "res", "quantity": 1},
                    "on": {
                        "target": {"id": c['_id'], "className": c['jsonClass']},
                        "path": [
                            {"meta_task_id": reg_task_info['task_id'], "target": {"id": res_id}},
                            {"target": {"id": c['_id']}}
                        ]
                    },
                    "status": {
                        "id": task_id,
                        "url": os.path.join(task_route_base, task_id),
                        "state": "PENDING",
                    },
                    "actions": ["read", "updateContent"],
                    "service": {
                        "name": "sleeper"
                    },
                }
                iter_tasks.append(task)
        except ObjModelException as e:
            pass

        if len(iter_tasks):
            iter_reg_task_infos = vv_context.task_manager.register_tasks(
                iter_tasks, agent_context
            )
            for i, rti in enumerate(iter_reg_task_infos):
                child = children[i]
                task = iter_tasks[i]
                if rti['status'] == 'FAILURE':
                    failed_children.append(child['_id'])
                    continue
                sleep_for_res_task.apply_async(
                    [vv_context_descriptor, agent_context, rti['info']],
                    task_id=glom.glom(task, 'status.id')
                )
                succeded_children.append(child['_id'])

        cumulative_count += count
        self.update_state(
            state='PROGRESS',
            meta={
                "status": "launching sub tasks",
                "current": cumulative_count, "total": total_count,
                "succeeded": len(succeded_children), "failed": len(failed_children)}
        )

    success_meta = {
        "status": "sub tasks launching completed",
        "current": cumulative_count, "total": total_count,
        "succeeded": len(succeded_children), "failed": len(failed_children)}

    vv_context.task_manager.update_task_status(
        reg_task_info['task_token'], 'SUCCESS', task_meta=success_meta)  # meta task status update

    return success_meta
