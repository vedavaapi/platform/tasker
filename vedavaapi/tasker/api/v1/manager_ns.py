from flask import g
import flask_restx

from vedavaapi.common.helpers.api_helper import error_response, abort_with_error_response
from vedavaapi.common.helpers.args_parse_helper import parse_json_args
from vedavaapi.common.helpers.token_helper import require_oauth, current_token
from vedavaapi.objectdb.helpers import ObjModelException, projection_helper

from . import api
from ... import TaskManager


manager_ns = api.namespace('manager', path='/manager')


def _validate_projection(projection):
    try:
        projection_helper.validate_projection(projection)
    except ObjModelException as e:
        error = error_response(message=e.message, code=e.http_response_code)
        abort_with_error_response(error)


@manager_ns.route('/tasks')
class Tasks(flask_restx.Resource):
    get_parser = manager_ns.parser()
    get_parser.add_argument(
        'selector_doc', location='args', type=str, default='{}',
        help='syntax is same as mongo query_doc. https://docs.mongodb.com/manual/tutorial/query-documents/'
    )
    get_parser.add_argument('projection', location='args', type=str, help='ex: {"permissions": 0}')
    get_parser.add_argument('sort_doc', location='args', type=str, help='ex: [["created", 1], ["title.chars", -1]]')
    get_parser.add_argument('start', location='args', type=int, default=0)
    get_parser.add_argument('count', location='args', type=int)
    get_parser.add_argument('return_count_only', location='args', type=str, choices=['true', 'false'], default='false')
    get_parser.add_argument(
        'Authorization', location='headers', type=str, required=True,
        help='should be in form of "Bearer <access_token>"'
    )

    get_payload_json_parse_directives = {
        "selector_doc": {
            "allowed_types": (dict,), "default": {}
        },
        "projection": {
            "allowed_types": (dict,), "allow_none": True, "custom_validator": _validate_projection
        },
        "sort_doc": {
            "allowed_types": (dict, list), "allow_none": True
        },
        "return_count_only": {
            "allowed_types": (bool,), "default": False
        }
    }

    post_parser = manager_ns.parser()
    post_parser.add_argument('tasks', location='form', type=str, default='[]')
    post_parser.add_argument(
        'Authorization', location='headers', type=str, required=True,
        help='should be in form of "Bearer <access_token>"'
    )

    post_payload_json_parse_directives = {
        "tasks": {"allowed_types": (list,), "allow_none": False, "required": True},
    }

    put_parser = manager_ns.parser()
    put_parser.add_argument(
        'state', location='form', type=str, choices=TaskManager.STATES, required=True)
    put_parser.add_argument(
        'meta', location='form', type=str, default='{}')
    put_parser.add_argument('work_update', location='form', type=str, default='{}')
    put_parser.add_argument(
        'task_token', location='form', type=str, required=True
    )
    put_parser.add_argument(
        'Authorization', location='headers', type=str, required=True,
        help='should be in form of "Bearer <access_token>"'
    )

    put_payload_json_parse_directives = {
        "work_update": {"allowed_types": (dict,), "allow_none": False, "required": False},
        "meta": {"allowed_types": (dict,), "allow_none": False, "required": False},
    }

    @manager_ns.expect(get_parser, validate=True)
    @require_oauth(token_required=True)
    def get(self):
        args = self.get_parser.parse_args()
        args = parse_json_args(args, self.get_payload_json_parse_directives)
        task_manager = g.task_manager  # type: TaskManager

        try:
            return task_manager.get_tasks(
                args['selector_doc'], args['start'], args['count'],
                args.get('sort_doc', None), current_token,
                projection=args.get('projection'),
                return_count_only=args.get('return_count_only')
            )
        except ObjModelException as e:
            return error_response(message=e.message, code=e.http_response_code)

    @manager_ns.expect(post_parser, validate=True)
    @require_oauth(token_required=True)
    def post(self):
        args = parse_json_args(
            self.post_parser.parse_args(), self.post_payload_json_parse_directives)
        tasks = args['tasks']
        for task in tasks:
            if not isinstance(task, dict):
                return error_response(message='task should be a dict', code=400)

        task_manager = g.task_manager  # type: TaskManager
        try:
            reg_task_infos = task_manager.register_tasks(
                tasks, current_token
            )
            return reg_task_infos
        except ObjModelException as e:
            return error_response(message=e.message, code=e.http_response_code)

    @manager_ns.expect(put_parser, validate=True)
    def put(self):
        args = parse_json_args(
            self.put_parser.parse_args(), self.put_payload_json_parse_directives)

        auth_header = args.get('Authorization')  # type: str
        if not auth_header or not auth_header.startswith('Bearer '):
            return error_response(message='invalid authorization header', code=403)

        task_token = args['task_token']
        task_manager = g.task_manager  # type: TaskManager

        try:
            task_manager.update_task_status(
                task_token, args['state'],
                task_meta=args.get('meta'), work_update=args.get('work_update')
            )
            return {"state": "SUCCESS"}
        except ObjModelException as e:
            return error_response(message=e.message, code=e.http_response_code)


@manager_ns.route('/stats')
class Stats(flask_restx.Resource):
    get_parser = manager_ns.parser()
    get_parser.add_argument(
        'selector_docs', location='args', type=str, required=True,
        help='syntax is same as mongo query_doc. https://docs.mongodb.com/manual/tutorial/query-documents/'
    )
    get_parser.add_argument(
        'Authorization', location='headers', type=str, required=True,
        help='should be in form of "Bearer <access_token>"'
    )

    get_payload_json_parse_directives = {
        "selector_docs": {
            "allowed_types": (list,), "default": {},
        }
    }

    @manager_ns.expect(get_parser, validate=True)
    @require_oauth(token_required=True)
    def get(self):
        args = self.get_parser.parse_args()
        args = parse_json_args(args, self.get_payload_json_parse_directives)
        task_manager = g.task_manager  # type: TaskManager

        counts = []
        try:
            for selector_doc in args['selector_docs']:
                if not isinstance(selector_doc, dict):
                    return error_response(message='invalid selector docs', code=400)
                count = task_manager.tasks_colln.count(selector_doc)
                counts.append(count)
        except Exception as e:
            return error_response(
                message='invalid args?', code=400, error=getattr(e, 'message', 'Error'))

        return {'counts': counts}
