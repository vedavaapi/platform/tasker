import uuid

import flask_restx
from flask import g


from vedavaapi.common.helpers.api_helper import error_response
from vedavaapi.common.helpers.args_parse_helper import parse_json_args
from vedavaapi.common.helpers.models import AgentContext
from vedavaapi.common.helpers.token_helper import require_oauth, current_token

from vedavaapi.common.helpers import celery_helper
from vedavaapi.objectdb.helpers import ObjModelException

from ... import TaskManager
from ...lib.mocker.worker import sleep_for_res_task, sleep_for_children_task
from . import api


mocker_ns = api.namespace('mocker', path='/mocker')


@mocker_ns.route('/sleeper')
class Sleeper(flask_restx.Resource):

    post_parser = mocker_ns.parser()
    post_parser.add_argument('res_id', type=str, location='form', required=True)
    post_parser.add_argument(
        'Authorization', location='headers', type=str, required=True,
        help='should be in form of "Bearer <access_token>"'
    )

    @mocker_ns.expect(post_parser, validate=True)
    @require_oauth()
    def post(self):
        args = self.post_parser.parse_args()
        task_id = uuid.uuid4().hex
        rtask = {
            "jsonClass": "Task",
            "type":"rdfs:Resource",  # TODO
            "name": "Mock task",
            "work": {"work_type": "sleep", "quantum": "res", "quantity": 1},
            "on": {
                "target": {"id": args['res_id']},
                "path": [
                    {"target": {"id": args['res_id']}}
                ]
            },
            "status": {
                "id": task_id,
                "url": api.url_for(CeleryTask, task_id=task_id, _external=True),
                "state": "PENDING",
            },
            "actions": ["read", "updateContent"],
            "service": {
                "name": "sleeper"
            },
        }
        agent_context = AgentContext(current_token.user_id, current_token.team_ids)
        task_manager = g.task_manager  # type: TaskManager

        try:
            task_info = task_manager.register_task(rtask, agent_context)
            sleep_for_res_task.apply_async(
                [task_manager.vv_context.get_descriptor(), agent_context, task_info],
                task_id=task_id
            )
            task_status_url = api.url_for(CeleryTask, task_id=task_id, _external=True)
            return {
                   "status_url": task_status_url,
                   "task_id": task_id, "reg_id": task_info['task_id']}, 202, {
                'Location': task_status_url, 'Access-Control-Expose-Headers': 'Location'
            }
        except ObjModelException as e:
            return error_response(
                message='error in registering task', code=400, error={"reason": e.message})


@mocker_ns.route('/children-sleeper')
class ChildrenSleeper(flask_restx.Resource):

    post_parser = mocker_ns.parser()
    post_parser.add_argument('res_id', type=str, location='form', required=True)
    post_parser.add_argument(
        'filter_doc', type=str, location='form', required=False, default='{}')
    post_parser.add_argument(
        'Authorization', location='headers', type=str, required=True,
        help='should be in form of "Bearer <access_token>"'
    )

    post_payload_json_parse_directives =  {
        "filter_doc": {
            "allowed_types": (dict, ), "allow_none": False, "required": True, "dafault": {}},
    }

    @mocker_ns.expect(post_parser, validate=True)
    @require_oauth()
    def post(self):
        args = self.post_parser.parse_args()
        args = parse_json_args(args, self.post_payload_json_parse_directives)
        task_id = uuid.uuid4().hex
        rtask = {
            "jsonClass": "Task",
            "type":"rdfs:Resource",
            "name": "Mock meta task",
            "work": {"work_type": "sub-launcher"},
            "on": {
                "target": {"id": args['res_id']},
                "path": [
                    {"target": {"id": args['res_id']}}
                ]
            },
            "status": {
                "id": task_id,
                "url": api.url_for(CeleryTask, task_id=task_id, _external=True),
                "state": "PENDING",
            },
            "actions": ["read", "updateContent"],
            "service": {
                "name": "sleeper"
            },
        }
        agent_context = AgentContext(current_token.user_id, current_token.team_ids)
        task_manager = g.task_manager  #  type: TaskManager

        try:
            task_info = task_manager.register_task(rtask, agent_context)
            sleep_for_children_task.apply_async(
                [task_manager.vv_context.get_descriptor(), agent_context, task_info,
                 args['res_id'], args.get('filter_doc', {}),
                 api.url_for(CeleryTask, task_id='', _external=True)],
                task_id=task_id
            )
            task_status_url = api.url_for(CeleryTask, task_id=task_id, _external=True)
            return {
                    "status_url": task_status_url,
                    "task_id": task_id, "reg_id": task_info['task_id']}, 202, {
                'Location': task_status_url, 'Access-Control-Expose-Headers': 'Location'
            }
        except ObjModelException as e:
            return error_response(
                message='error in registering task', code=400, error={"reason": e.message})


@mocker_ns.route('/tasks/<task_id>')
class CeleryTask(flask_restx.Resource):

    # noinspection PyMethodMayBeStatic
    def get(self, task_id):
        from vedavaapi.common.helpers.celery import celery as celery_app
        return celery_helper.get_task_status(celery_app, task_id)

    # noinspection PyMethodMayBeStatic
    def delete(self, task_id):
        from vedavaapi.common.helpers.celery import celery as celery_app
        celery_helper.terminate_task(celery_app, task_id)
        return {"task_id": task_id, "success": True}
