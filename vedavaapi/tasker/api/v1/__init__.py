import flask_restx
from flask import Blueprint

from .. import myservice


api_blueprint_v1 = Blueprint(myservice().name, __name__)


api = flask_restx.Api(
    app=api_blueprint_v1,
    version='1.0',
    title=myservice().title,
    description=myservice().description,
    doc='/docs'
)

from . import manager_ns
from . import mock_ns
