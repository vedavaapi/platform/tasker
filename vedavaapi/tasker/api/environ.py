import os
from flask import g

from vedavaapi.common.helpers.api_helper import get_current_org

from . import myservice


def _get_token_introspection_endpoint():
    current_org_name = get_current_org()
    accounts_api_config = myservice().get_accounts_api_config(current_org_name)

    url_root = accounts_api_config.get('url_root', g.original_url_root)
    token_resolver_endpoint = os.path.join(
        url_root.lstrip('/'),
        current_org_name,
        'accounts/v1/oauth/introspect_token'
    )
    return token_resolver_endpoint


def push_environ_to_g():
    #  g.token_introspection_endpoint = _get_token_introspection_endpoint()
    g.task_manager = myservice().task_manager(get_current_org())
