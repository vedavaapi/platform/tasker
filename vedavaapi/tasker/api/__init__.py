from vedavaapi.common.helpers.api_helper import get_current_org

from .. import VedavaapiTasker


def myservice():
    return VedavaapiTasker.instance


def get_colln():
    current_org_name = get_current_org()
    return myservice().resolver(current_org_name)


from . import environ

from .v1 import api_blueprint_v1
api_blueprint_v1.before_request(environ.push_environ_to_g)


blueprints_path_map = {
    api_blueprint_v1: "/v1"
}
