from vedavaapi.common import VedavaapiService, OrgHandler
from vedavaapi.common.helpers.models import VVContext
from vedavaapi.objectdb.mydb import MyDbCollection

from .lib import TaskManager


class TaskerOrgHandler(OrgHandler):

    def __init__(self, service, org_name):
        super(TaskerOrgHandler, self).__init__(service, org_name)
        self._resolver = None

        self.db = self.store.db(f'{self.org_name}_{self.service.name}')
        self.colln = self.db.get_collection('tasks')  # type: MyDbCollection
        self.objstore_colln = self.service.objstore_svc.colln(self.org_name)
        self.task_manager = TaskManager(
            VVContext(
                self.objstore_colln, None,
                self.service.registry.lookup('acls').get_acl_svc(self.org_name),
                self.service.registry.lookup('schemas').get_schema_validator(
                    self.org_name, self.objstore_colln),
                tasks_colln=self.colln
            ),
            self.colln,
        )

    def initialize(self):
        self.colln.create_index({"on.target": 1}, 'target_link')
        self.colln.create_index({"on.path": 1}, 'path_link')
        self.colln.create_index({"agent.user.id": 1}, 'user_link')
        self.colln.create_index({"service.name": 1}, 'service_name')


class VedavaapiTasker(VedavaapiService):

    instance = None  # type: VedavaapiTasker

    dependency_services = ['objstore']
    org_handler_class = TaskerOrgHandler

    title = 'Tasker'
    description = 'Centralized task manager'

    def __init__(self, registry, name, conf):
        super(VedavaapiTasker, self).__init__(registry, name, conf)
        self.objstore_svc = self.registry.lookup('objstore')

    def init_service(self):
        for org_name in self.registry.org_names:
            self.get_org(org_name)  # initializes all orgs.

    def colln(self, org_name):
        return self.get_org(org_name).colln  # type: MyDbCollection

    def task_manager(self, org_name):
        return self.get_org(org_name).task_manager  #type: TaskManager
