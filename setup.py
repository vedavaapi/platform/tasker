from setuptools import setup

try:
    with open("README.md", "r") as fh:
        long_description = fh.read()
except:
    long_description = ''

setup(
    name='tasker',
    version='1.0.0',
    packages=['vedavaapi', 'vedavaapi.tasker'],
    url='https://github.com/vedavaapi',
    author='vedavaapi',
    description='Task manager',
    long_description=long_description,
    long_description_content_type="text/markdown",
    install_requires=['flask', 'flask-restx'],
    classifiers=(
            "Programming Language :: Python :: 2.7",
            "Operating System :: OS Independent",
    )
)
